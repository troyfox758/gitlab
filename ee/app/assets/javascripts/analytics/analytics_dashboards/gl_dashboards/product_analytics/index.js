export default {
  behavior: () => import(`./dashboard_behavior.json`),
  audience: () => import(`./dashboard_audience.json`),
};
